Ajouter un objet chargeable : trucs à toucher : 
	-> structs.h : Ajouter le nom de l'objet dans Objects_name.
	-> objects.C : 
		-> object_load_tile() : ajouter la tile de l'objet associée au nom renseigné dans Objetcts_name en suivant le modèle existant.
		-> object_get_interaction_zone() : ajouter les infos sur le point d'interaction de l'objet et sa portée en fonction du nom de l'objet.
		-> pour l'instant c'est tout.


Charger un objet : l'appeler dans map.c ou story.c avec : 

Object* object_load(Objects_name name, short x, short y, int map_id, int unique_id, short flag);

name : nom de l'objet
x et y les coordonnées du coin haut gauche de l'objet.
map_id : l'id de la map (x_map << 16 | y_map), un peu inutile pour l'instant.
unique_id : laisser à 0 pour l'instant, encore inutile.
flag : suivre le modèle : permet d'assigner des propriétés à l'objet : par exemple : objet physique (collisionable) et déplaçable (pas géré encore): 
flag = OBJECT_FLAG_PHYSIC|OBJECT_FLAG_MOVABLE