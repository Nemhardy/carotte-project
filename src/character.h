#ifndef CHARACTER_H

#define CHARACTER_H

#include "structs.h"

#define ANIM_MILLISECONDS 100

// Loads the requested Character in memory
Character* character_load(int id);

//Asigns the correct tiles_name to the correct objects_name
Tile* character_get_tile(Objects_name name);

Tile* character_tile_load(Tiles_name name);

// May become static soon.
Character* character_create(Tile** tiles, int tiles_number, short width, short height, char anim_max);

void character_next_frame(Character* character);

void character_set_tile(Character* character, const Direction direction);

void character_reset_frame(Character* character);

// Frees the character in parameter.
void character_free(Character* character);

#endif // CHARACTER_H