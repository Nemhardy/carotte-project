#ifndef _CC_FILE

#define _CC_FILE

// Simple interface between syscalls and programmer-side simpler functions

#define ROOT "\\\\fls0\\"

#define CURRENT_POSITION -1

#define CREATEMODE_FILE 1
#define CREATEMODE_FOLDER 5
#define READ 0
#define READ_SHARE 1
#define WRITE 2
#define READWRITE 3
#define READWRITE_SHARE 4

#define SEARCH_NOTFOUND 0
#define SEARCH_FOUND 1

typedef struct {
	unsigned short id, type;
	unsigned long fsize, dsize;
	unsigned int property;
	unsigned long address;
} file_type_t;


void CC_make_path(unsigned short* destination, const unsigned char* file);
int CC_make_folder(unsigned char* folder);
int CC_make_file(unsigned char* folder, int size);

int CC_open_file(unsigned char* folder);
int CC_close_file(int handle);

int CC_write_file(int handle, const void *buf, int size);
int CC_read_file(int handle, void* destination, int size, int position);

//#Syscalls
void CC_Sys_StrToName( unsigned short*dest, const unsigned char*source, int n );

int CC_Sys_CreateEntry( const unsigned short* filename, int mode, int*size );

int CC_Sys_OpenFile( const unsigned short* filename, int mode );
int CC_Sys_CloseFile( int HANDLE );

int CC_Sys_ReadFile( int HANDLE, void *buf, int size, int readpos ); 
int CC_Sys_WriteFile( int HANDLE, const void *buf, int size );

#endif //_CC_FILE