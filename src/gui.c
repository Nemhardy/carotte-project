#include <stdlib.h>
#include <keyboard.h>
#include <system.h>
#include <display.h>
#include <color.h>

#include "structs.h"

#include "gui.h"

#include "objects.h"
#include "graphic_functions.h"
#include "story.h"
#include "text.h"

void gui_draw_game_zone(Map* map, Player* player)
{
	Object* object_interact = NULL;

	object_interact = object_get_nearest_interaction(map, player);
	if(object_interact) text_print(object_interact->x, object_interact->y, "Interact", 1, 0x0010FFFF);
}

void gui_draw_bar()
{
	int quest_consulted=0, i; // if never consulted : show a special sprite
	
	const unsigned char bag[]={
		0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0, 
		0, 3, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 3, 0, 0, 
		0, 3, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 3, 0, 0, 
		0, 3, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 3, 0, 0, 
		0, 3, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 3, 0, 0, 
		0, 3, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 3, 0, 0, 
		0, 3, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 3, 0, 0, 
		0, 3, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 3, 0, 0, 
		0, 3, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 3, 0, 0, 
		0, 3, 3, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 0, 0, 0, 0, 0, 0, 3, 3, 0, 0, 
		0, 3, 3, 3, 3, 3, 3, 3, 3, 3, 0, 0, 0, 3, 3, 3, 3, 3, 3, 3, 3, 0, 0, 
		0, 3, 3, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 3, 3, 3, 3, 3, 3, 3, 3, 0, 0, 
		0, 3, 3, 3, 3, 3, 3, 3, 3, 3, 5, 5, 5, 3, 3, 3, 3, 3, 3, 3, 3, 0, 0, 
		0, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 0, 0, 
		0, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 0, 0, 
		0, 0, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 0, 0, 0, 
		0, 0, 0, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
	};
	const short palette_bag[6]={
		0x0, 0x48e0, 0xaa20, 0x7980, 0xd580, 0xb4c0
	};

	const unsigned char map[]={
		0, 0, 0, 0, 0, 0, 1, 2, 3, 4, 4, 4, 4, 4, 4, 5, 6, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 7, 8, 4, 4, 4, 9, 0, 10, 0, 8, 4, 4, 4, 9, 1, 0, 0, 0, 0, 
		0, 0, 0, 11, 4, 4, 12, 13, 14, 14, 0, 0, 0, 14, 14, 14, 9, 4, 4, 13, 0, 0, 0, 
		0, 0, 11, 4, 4, 15, 14, 14, 14, 14, 0, 14, 0, 14, 14, 14, 14, 14, 4, 4, 15, 0, 0, 
		0, 1, 4, 4, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 3, 4, 16, 0, 
		0, 12, 4, 14, 14, 14, 17, 14, 14, 14, 14, 0, 14, 14, 14, 14, 14, 14, 14, 14, 4, 4, 18, 
		16, 4, 19, 14, 14, 14, 14, 17, 14, 14, 14, 0, 14, 14, 14, 14, 14, 14, 14, 14, 13, 4, 2, 
		9, 4, 14, 14, 14, 14, 14, 14, 17, 14, 14, 0, 14, 14, 14, 14, 14, 14, 14, 14, 14, 4, 4, 
		4, 12, 14, 14, 14, 14, 14, 14, 14, 17, 17, 0, 14, 0, 14, 14, 14, 14, 14, 14, 14, 10, 4, 
		4, 0, 0, 0, 14, 14, 14, 14, 14, 17, 17, 20, 0, 14, 14, 14, 14, 14, 14, 0, 0, 13, 4, 
		4, 0, 14, 0, 14, 0, 0, 0, 0, 0, 20, 21, 20, 0, 0, 0, 0, 0, 14, 0, 14, 14, 4, 
		4, 0, 0, 0, 14, 14, 14, 14, 14, 14, 0, 20, 22, 22, 14, 14, 14, 14, 14, 0, 0, 14, 4, 
		4, 8, 14, 14, 14, 14, 14, 14, 14, 0, 14, 0, 22, 22, 14, 14, 14, 14, 14, 14, 14, 2, 4, 
		3, 4, 14, 14, 14, 14, 14, 14, 14, 14, 14, 0, 14, 14, 22, 14, 14, 14, 14, 14, 14, 3, 4, 
		15, 4, 13, 14, 14, 14, 14, 14, 14, 14, 14, 0, 14, 14, 14, 22, 14, 14, 14, 14, 14, 4, 19, 
		7, 4, 4, 14, 14, 14, 14, 14, 14, 14, 14, 0, 14, 14, 14, 14, 22, 14, 14, 14, 12, 4, 1, 
		0, 13, 4, 12, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 8, 4, 10, 0, 
		0, 0, 10, 4, 3, 14, 14, 14, 14, 14, 0, 0, 0, 14, 14, 14, 14, 14, 9, 4, 9, 0, 0, 
		0, 0, 0, 19, 4, 4, 15, 14, 14, 14, 14, 0, 14, 14, 14, 14, 13, 4, 4, 9, 18, 0, 0, 
		0, 0, 0, 0, 16, 4, 4, 4, 8, 13, 0, 0, 0, 14, 19, 4, 4, 4, 15, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 0, 4, 4, 4, 4, 4, 4, 4, 4, 4, 0, 0, 0, 0, 0, 0, 0, 
	};
	const short palette_map[23]={
		0x0, 0x1082, 0x2144, 0x39c7, 0x39e7, 0x2945, 0x10a2, 0x841, 0x3186, 0x31a6, 0x2965, 0x18c3, 0x31c6, 0x2104, 0x7bef, 0x2124, 0x18e3, 0xf822, 0x20, 0x2985, 0xfda3, 0xfec2, 0xffff
	};
	
	const unsigned char journaux[]={
		0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 3, 4, 5, 6, 7, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 0, 8, 9, 10, 11, 12, 13, 14, 15, 2, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 16, 17, 18, 19, 19, 20, 21, 21, 22, 23, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 2, 24, 19, 19, 25, 26, 27, 28, 29, 30, 23, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 31, 32, 19, 19, 18, 33, 34, 35, 8, 36, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 6, 18, 19, 19, 21, 37, 38, 39, 40, 41, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 42, 19, 19, 19, 43, 44, 45, 0, 46, 47, 48, 41, 1, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 49, 19, 19, 19, 17, 13, 50, 13, 51, 52, 53, 54, 55, 56, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 57, 19, 19, 19, 19, 19, 19, 58, 59, 60, 19, 18, 61, 47, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 32, 19, 25, 25, 62, 25, 19, 49, 63, 64, 65, 66, 67, 68, 0, 0, 0, 0, 
		0, 0, 0, 0, 69, 21, 70, 71, 72, 73, 25, 19, 31, 17, 74, 75, 76, 40, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 77, 11, 78, 79, 79, 80, 60, 81, 0, 82, 83, 40, 84, 33, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 63, 58, 85, 15, 3, 86, 18, 87, 88, 89, 90, 47, 91, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 14, 58, 92, 22, 93, 94, 19, 67, 60, 95, 29, 96, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 14, 18, 97, 80, 95, 98, 19, 36, 58, 99, 100, 101, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 63, 18, 58, 102, 25, 58, 21, 94, 18, 103, 23, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 77, 58, 25, 104, 73, 105, 106, 107, 73, 2, 108, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 36, 58, 109, 110, 111, 68, 1, 59, 5, 108, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 5, 50, 112, 21, 113, 96, 114, 5, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 47, 115, 116, 117, 118, 119, 6, 68, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 56, 5, 1, 120, 121, 68, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
	};
	const short palette_journaux[122]={
		0x0, 0x2104, 0x2945, 0x2123, 0x2144, 0x1082, 0x18e3, 0x31a6, 0x4a68, 0x73cc, 0x8c4e, 0xdf15, 0xb5b2, 0x842d, 0x6b8b, 0x3185, 0x4207, 0xe736, 0xf7d8, 0xfff9, 0xf7db, 0xef98, 0x840d, 0x39e7, 0x4a67, 0xf7d9, 0xd6d8, 0x5267, 0x20a1, 0x39c5, 0x73ac, 0x861, 0xef77, 0x8430, 0x9454, 0xd409, 0x632a, 0x5206, 0xd48e, 0x5b33, 0x2124, 0x18c2, 0xa550, 0xdef5, 0x38a2, 0xa1a6, 0xad55, 0x10a2, 0x2103, 0xb5d2, 0x8c8e, 0x52ca, 0x94cf, 0xdf16, 0xbdd1, 0x2964, 0x18c3, 0xc613, 0xf7b8, 0x39e6, 0xe757, 0x738b, 0xef97, 0x6b6b, 0xce74, 0xce99, 0xef78, 0x5ae9, 0x39c7, 0x6309, 0xe759, 0x7bed, 0x73ce, 0x7bec, 0xa531, 0x7bcf, 0x630c, 0x6b4a, 0xbdf2, 0x9cee, 0xa52f, 0x8c6e, 0xb5b1, 0x2944, 0x3186, 0xbdf6, 0x842f, 0x6b4b, 0xd694, 0xd6d4, 0xc632, 0xb596, 0xc635, 0x73ab, 0xef7a, 0xbe12, 0x31c6, 0xd6b5, 0xf7b9, 0xb591, 0x7bef, 0x4a89, 0xf7bb, 0x844d, 0xc633, 0xa551, 0xbdf4, 0xe737, 0x4208, 0xc653, 0x5b0b, 0x2965, 0x39c6, 0x5ac8, 0x6b8d, 0x3a07, 0x9d11, 0xd6b8, 0xd6da, 0x8c90, 0x2985, 0x4a49
	};

	const unsigned char quit[]={
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 3, 4, 4, 1, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 5, 5, 2, 3, 4, 4, 1, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 2, 2, 6, 6, 6, 5, 5, 2, 3, 4, 4, 1, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 2, 6, 6, 5, 6, 6, 6, 5, 5, 2, 3, 4, 1, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 2, 6, 5, 5, 6, 6, 6, 6, 6, 6, 6, 2, 1, 7, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 2, 6, 5, 6, 6, 6, 6, 6, 6, 6, 2, 2, 8, 7, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 2, 2, 6, 6, 6, 6, 6, 6, 2, 2, 2, 7, 8, 7, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 7, 2, 2, 6, 6, 6, 2, 2, 2, 7, 7, 0, 7, 7, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 8, 7, 2, 2, 2, 2, 2, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 7, 2, 2, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 0, 9, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 0, 8, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 0, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
	};
	const short palette_quit[10]={
		0x0, 0x7bef, 0x7800, 0xa514, 0xffff, 0xc9a6, 0xb000, 0x7980, 0x91e0, 0xba40
	};

	const unsigned char quest[]={
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 2, 4, 5, 6, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 0, 0, 7, 2, 8, 9, 10, 11, 12, 13, 0, 7, 14, 0, 0, 0, 0, 
		0, 0, 0, 0, 15, 16, 2, 17, 8, 9, 18, 11, 11, 19, 20, 21, 22, 23, 2, 0, 0, 0, 0, 
		0, 0, 0, 24, 17, 25, 26, 27, 28, 11, 11, 29, 30, 31, 26, 32, 33, 33, 2, 0, 0, 0, 0, 
		0, 0, 0, 2, 25, 34, 35, 36, 37, 38, 39, 40, 8, 8, 8, 41, 33, 33, 2, 0, 0, 0, 0, 
		0, 0, 0, 2, 5, 42, 11, 11, 43, 8, 8, 8, 8, 8, 8, 8, 33, 33, 2, 0, 0, 0, 0, 
		0, 0, 0, 44, 4, 38, 11, 11, 26, 9, 8, 8, 8, 8, 8, 8, 8, 45, 7, 0, 0, 0, 0, 
		0, 0, 0, 0, 46, 26, 47, 48, 8, 27, 8, 26, 26, 26, 8, 8, 8, 8, 7, 0, 0, 0, 0, 
		0, 0, 0, 0, 49, 33, 50, 50, 51, 52, 8, 33, 33, 53, 8, 8, 8, 8, 13, 0, 0, 0, 0, 
		0, 0, 0, 0, 54, 55, 33, 33, 45, 56, 45, 33, 57, 8, 8, 8, 8, 8, 58, 0, 0, 0, 0, 
		0, 0, 0, 0, 59, 4, 9, 9, 25, 60, 61, 62, 25, 8, 8, 8, 8, 8, 63, 0, 0, 0, 0, 
		0, 0, 0, 0, 64, 17, 8, 8, 8, 5, 65, 8, 8, 8, 8, 8, 8, 8, 66, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 2, 8, 8, 8, 8, 9, 8, 8, 8, 8, 8, 8, 26, 44, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 67, 26, 8, 8, 8, 9, 8, 8, 8, 8, 8, 25, 68, 69, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 2, 8, 8, 8, 9, 25, 33, 33, 26, 8, 50, 70, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 2, 8, 8, 25, 71, 51, 33, 72, 8, 8, 33, 17, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 73, 74, 51, 33, 75, 76, 77, 26, 2, 2, 78, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 79, 80, 33, 33, 81, 2, 2, 82, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 0, 2, 65, 53, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 0, 83, 2, 2, 83, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
	};
	const short palette_quest[84]={ 
		0x0, 0x1045, 0x6180, 0x3904, 0x69e0, 0x61e0, 0x5982, 0x5960, 0x7240, 0x51a0, 0xef17, 0xff38, 0x82c5, 0x5141, 0x5, 0x20a3, 0x4921, 0x61a0, 0xeef6, 0xff17, 0xf717, 0xcd0c, 0x7201, 0xcc86, 0x30e4, 0x6a20, 0x7220, 0x5180, 0xf799, 0xf610, 0xf737, 0xe5ae, 0xcca5, 0xfe40, 0xfe51, 0xf738, 0xff37, 0xfeb4, 0xfe30, 0xfe31, 0x9b65, 0xa3c0, 0xf630, 0xee10, 0x1885, 0xfe60, 0x7200, 0xc4aa, 0xedcf, 0x6160, 0xf660, 0xf640, 0x6200, 0x6a00, 0x822, 0xa3a0, 0x7aa2, 0xdda0, 0x4922, 0x823, 0xc445, 0xc4c0, 0x7aa0, 0x38e2, 0x3, 0x59c0, 0x28c4, 0x1866, 0xc4a0, 0x825, 0xb400, 0x8ae2, 0x9340, 0x5980, 0xe5c0, 0xe5a1, 0x8b20, 0x9380, 0x38e4, 0x4124, 0xdd80, 0x6220, 0x20c5, 0x20a4
	};
	
	const unsigned char quest_unconsulted[]={
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 2, 4, 5, 6, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 7, 8, 9, 10, 11, 12, 0, 2, 13, 0, 0, 0, 0, 
		0, 0, 0, 0, 14, 15, 2, 16, 17, 8, 18, 10, 10, 19, 20, 21, 22, 23, 2, 0, 0, 0, 0, 
		0, 0, 0, 24, 16, 22, 22, 25, 26, 10, 10, 27, 28, 29, 22, 30, 31, 31, 2, 0, 0, 0, 0, 
		0, 0, 0, 2, 22, 32, 33, 34, 35, 36, 37, 38, 7, 17, 17, 39, 31, 31, 2, 0, 0, 0, 0, 
		0, 0, 0, 2, 5, 40, 10, 10, 41, 7, 17, 17, 17, 17, 17, 7, 31, 31, 2, 0, 0, 0, 0, 
		0, 0, 0, 42, 4, 36, 10, 10, 22, 8, 17, 17, 17, 17, 17, 17, 7, 43, 2, 0, 0, 0, 0, 
		0, 0, 0, 0, 4, 22, 44, 45, 7, 25, 7, 22, 22, 22, 17, 17, 17, 17, 25, 0, 0, 0, 0, 
		0, 0, 0, 0, 2, 31, 46, 46, 47, 48, 7, 31, 31, 22, 17, 17, 17, 17, 49, 0, 0, 0, 0, 
		0, 0, 0, 0, 50, 39, 31, 31, 43, 51, 43, 31, 52, 17, 17, 17, 17, 17, 53, 0, 0, 0, 0, 
		0, 0, 0, 0, 54, 4, 8, 8, 22, 55, 56, 57, 22, 17, 17, 17, 17, 7, 58, 0, 0, 0, 0, 
		0, 0, 0, 0, 59, 16, 17, 17, 17, 5, 16, 7, 17, 17, 17, 17, 17, 7, 60, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 2, 17, 17, 17, 7, 8, 17, 17, 17, 17, 17, 17, 22, 61, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 62, 22, 17, 17, 7, 8, 17, 17, 17, 17, 17, 22, 63, 64, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 2, 17, 17, 17, 8, 22, 31, 31, 22, 17, 46, 65, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 2, 17, 17, 22, 66, 47, 31, 67, 17, 17, 31, 16, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 2, 68, 47, 31, 69, 70, 71, 22, 2, 2, 72, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 73, 74, 31, 31, 22, 2, 2, 75, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 0, 2, 8, 22, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0, 0, 76, 2, 2, 77, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
	};
	const short palette_quest_unconsulted[78]={
		0x0, 0x1045, 0x9ba1, 0x3904, 0xa3e1, 0x9be1, 0x93a2, 0xa421, 0x93c1, 0xef17, 0xff38, 0xb484, 0x9382, 0x5, 0x20a3, 0x9362, 0x9bc1, 0xac21, 0xeef6, 0xff17, 0xf717, 0xddc8, 0xa401, 0xdd85, 0x30e4, 0x93a1, 0xf799, 0xf610, 0xf737, 0xe5ae, 0xdd84, 0xfe40, 0xfe51, 0xf738, 0xff37, 0xfeb4, 0xfe30, 0xfe31, 0xbcc4, 0xc501, 0xf630, 0xee10, 0x1885, 0xfe60, 0xd587, 0xedcf, 0xf660, 0xf640, 0x9c01, 0x9381, 0x822, 0xac42, 0xdda0, 0x8b82, 0x823, 0xd544, 0xc4c0, 0xac41, 0x8342, 0x3, 0x7b43, 0x7304, 0x1866, 0xc4a0, 0x6ae4, 0xcd41, 0xb482, 0xbcc1, 0xe5c0, 0xe5a1, 0xb4a1, 0xbce1, 0x8343, 0x8b63, 0xdd80, 0x7b44, 0x7324, 0x7323
	};
	
	
	Quest* current_quest = get_current_quest();
	if(current_quest) quest_consulted = current_quest->consulted;

	FilledRectangle(0, 192, LCD_WIDTH_PX, LCD_HEIGHT_PX, 0, 0x0000, 0x0000);
	for(i=0; i<5; i++){
		i = i==4?5:i;
		Line(65*i, 192, 60+65*i, 192, 0x7980);
		Line(65*i, 193, 60+65*i, 193, 0x7980);
		Line(65*i, 193, 65*i, LCD_HEIGHT_PX, 0x7980);
		Line(65*i+1, 193, 65*i+1, LCD_HEIGHT_PX, 0x7980);
	}
	
	// Bag
	CopySpriteMasked(bag, palette_bag, 3, 195, 23, 21, 0x0000, 0);
	text_print(26, 205, "Bag", 1, 0x0000FFFF);
	// Quest
	if(quest_consulted) {
		CopySpriteMasked(quest, palette_quest, 68, 195, 23, 21, 0x0000, 0);
	} 
	else {
		CopySpriteMasked(quest_unconsulted, palette_quest_unconsulted, 68, 195, 23, 21, 0x0000, 0);
	}
	text_print(91, 205, "Quest", 1, 0x0000FFFF);
	// Map
	CopySpriteMasked(map, palette_map, 133, 195, 23, 21, 0x0000, 0);
	text_print(156, 205, "Map", 1, 0x0000FFFF);
	// Papers
	CopySpriteMasked(journaux, palette_journaux, 198, 195, 23, 21, 0x0000, 0);
	text_print(221, 205, "Papers", 1, 0x0000FFFF);
	// Quit
	CopySpriteMasked(quit, palette_quit, 328, 195, 23, 21, 0x0000, 0);
	text_print(351, 205, "Quit", 1, 0x0000FFFF);
}

void gui_update()
{
	int key;
	
	gui_draw_bar();
	
	if(key_down(GUI_KEY_QUEST)) {
		quest_set_consulted();
		gui_draw_quest();
		Bdisp_PutDisp_DD();
		OS_InnerWait_ms(250);
		GetKey(&key);
	}
}

void gui_draw_quest()
{
	Quest* quest = get_current_quest();
	const char* text = quest->text;
	const char* main_text = "Quete en cours";

	int size_1 = 2, size_2 = 1, radius = 6;
	int up_left_x = 15, up_left_y = 10;

	short bg_color = COLOR_MAROON;

	int middle_text_x = (LCD_WIDTH_PX/2) - (text_width_line(main_text)*size_1)/2;
	int middle_text_y = up_left_y + 10;
	int middle_width = text_width_line(main_text)*size_1;
	int middle_height = text_height_line(main_text)*size_1;

	int data_text_x = up_left_x + 20;
	int data_text_y = middle_text_y-5 + middle_height+text_height_line("a")*size_1*2;
	int data_width = LCD_WIDTH_PX-2*up_left_x - 2*(data_text_x-up_left_x);
	int data_height = LCD_HEIGHT_PX - 2*up_left_y - 2*(data_text_y-text_height_line("a")*size_1);

    RoundedSquareAlpha(up_left_x-1, up_left_y-1, LCD_WIDTH_PX-2*up_left_x+2, LCD_HEIGHT_PX-2*up_left_y+2, radius, 0x0000, 31);
	RoundedSquareAlpha(up_left_x, up_left_y, LCD_WIDTH_PX-2*up_left_x, LCD_HEIGHT_PX-2*up_left_y, radius, bg_color, 31);

	RoundedSquareAlpha(middle_text_x-text_widthC(' ')*size_1, middle_text_y-5, middle_width+2*text_widthC(' ')*size_1, middle_height+text_height_line("a")*size_1, radius, 0xFFFF, 32);
	text_print(middle_text_x, middle_text_y, main_text, size_1, 0x00000000);

	RoundedSquareAlpha(data_text_x-text_widthC(' ')*size_2, data_text_y-5, data_width, data_height, radius, 0xFFFF, 32);
	text_print(data_text_x, data_text_y, text, size_2, 0x00000000);
	//RoundedSquareAlpha((384)/2, (216)/2, 100, 40, 4, 0x8410, 32);
}