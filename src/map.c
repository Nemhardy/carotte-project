#include <stdlib.h>
#include <string.h>

#include "map.h"
#include "objects.h"
#include "text.h"
#include "tiles.h"
#include "memmgr.h"

#define MIN(a,b) (((a) < (b))? (a) : (b))
#define MAX(a,b) (((a) > (b))? (a) : (b))

Map* map_load(short x, short y)
{
	Map* map = NULL;
	int id = x<<16|y;
	if(x == 1 && y == 1) { //Example here...
		int standard_objects_number = 6;
		int objects_number = standard_objects_number + story_objects_number(id);
		Object** objects = alloca(objects_number*sizeof(Object*));
		Tiles_name tiles_list[3] = {SAND, GRASS, DIRT};
		char map_plan[] = {1,1,1,1,1,1,1,1,1,1,1,1,
							1,1,1,1,1,1,1,1,1,1,1,1,
							1,1,2,2,1,1,0,1,1,1,1,1,
							1,1,2,2,0,0,0,0,0,0,0,0,
							1,1,2,2,1,1,1,1,1,1,1,1,
							1,1,1,1,1,1,1,1,1,1,1,1};
		char map_angle[] = {0,0,0,0,0,0,0,0,0,0,0,0,
							0,0,0,0,0,0,0,0,0,0,0,0,
							0,0,0,0,0,0,0,0,0,0,0,0,
							0,0,0,0,0,0,0,0,0,0,0,0,
							0,0,0,0,0,0,0,0,0,0,0,0,
							0,0,0,0,0,0,0,0,0,0,0,0};
		map = map_create(tiles_list, 3, map_plan, map_angle, 12, 6, id, objects_number);

		objects[0] = object_load(SHELVES, 170, 5, id, 0, OBJECT_FLAG_PHYSIC|OBJECT_FLAG_MOVABLE); // Example
		objects[1] = object_load(PLACARD, 65, 50, id, 0, OBJECT_FLAG_PHYSIC|OBJECT_FLAG_MOVABLE); // Example
		objects[3] = object_load(TABLE, 100, 10, id, 0, OBJECT_FLAG_PHYSIC|OBJECT_FLAG_MOVABLE); // Example
		//objects[4] = object_load(PNJ_GIRL_DOWN, 100, 170, id, 0, OBJECT_FLAG_INTERACT|OBJECT_FLAG_PHYSIC); // Example
		//object_set_text(objects[4], "Moi c'est Cyndie la PNJ'ette\nJ'aime dire squadada.\nQuand je parle les gens m'ecoutent");
		objects[2] = object_load(PNJ_MECANO_UP, 250, 75, id, 0, OBJECT_FLAG_INTERACT|OBJECT_FLAG_PHYSIC); // Example
		object_set_text(objects[2], "Serge, mecano a plein temps, a votre service !");
		objects[4] = object_load(TREE, 300, 15, id, 0, OBJECT_FLAG_PHYSIC|OBJECT_FLAG_MOVABLE); // Example
		objects[5] = object_load(PNJ_GIRL_LEFT, 100, 100, id, 0, OBJECT_FLAG_INTERACT|OBJECT_FLAG_PHYSIC); // Example
		object_set_text(objects[5], "Moi c'est JP le PNJ !\nJ'aime dire squadada.\nJe m'etends meme sur une troisieme ligne");
		story_objects_complete(id, objects, standard_objects_number);

		object_map_copy(objects, map, objects_number);
	}
	else if(x == 2 && y == 1) {
		int objects_number = 3;
		Object* objects[3];
		Tiles_name tiles_list[] = {SAND, GRASS, DIRT};
		char map_plan[] = {0,1,1,1,1,0,1,0,1,0,2,1,
							0,1,1,1,1,2,1,0,1,0,1,2,
							0,1,2,1,1,2,1,0,1,0,1,2,
							0,1,1,1,1,0,1,0,1,2,1,2,
							0,0,1,1,1,2,1,0,1,0,1,2,
							0,1,1,1,1,0,1,0,1,0,2,0};
		char map_angle[] = {0,0,0,0,0,0,0,0,0,0,0,0,
							0,0,0,0,0,0,0,0,0,0,0,0,
							0,0,0,0,0,0,0,0,0,0,0,0,
							0,0,0,0,0,0,0,0,0,0,0,0,
							0,0,0,0,0,0,0,0,0,0,0,0,
							0,0,0,0,0,0,0,0,0,0,0,0};
		map = map_create(tiles_list, 3, map_plan, map_angle, 12, 6, id, objects_number);

		objects[0] = object_load(PNJ_GIRL_LEFT, 100, 100, id, 0, OBJECT_FLAG_INTERACT|OBJECT_FLAG_PHYSIC); // Example
		object_set_text(objects[0], "Moi c'est JP le PNJ !\nJ'aime dire squadada.\nJe m'etends meme sur une troisieme ligne");
		objects[1] = object_load(HOUSE_BAMBOO, 200, 100, id, 0, OBJECT_FLAG_PHYSIC|OBJECT_FLAG_MOVABLE); // Example
		objects[2] = object_load(TABLE, 100, 10, id, 0, OBJECT_FLAG_PHYSIC|OBJECT_FLAG_MOVABLE); // Example
		
		object_map_copy(objects, map, objects_number);
	}
	else if(x == 3 && y == 1) {
		int objects_number = 0;
		Object* objects[2];
		Tiles_name tiles_list[] = {DIRT, GRASS, DIRT_GRASS_LINE, DIRT_GRASS_TURN_INT, DIRT_GRASS_TURN_EXT};
		char map_plan[] = {	2,1,1,1,1,1,2,0,0,0,0,0,
							2,1,1,1,1,1,3,2,4,0,0,0,
							2,1,1,1,1,1,1,1,2,0,0,0,
							2,1,1,1,1,1,1,1,2,0,0,0,
							4,2,2,2,2,2,2,2,4,0,0,0,
							0,0,0,0,0,0,0,0,0,0,0,0};
		unsigned char map_angle[] = {0,0,0,0,0,0,A_180,0,0,0,0,0,
							0,0,0,0,0,0,A_180,A_90,A_270,0,0,0,
							0,0,0,0,0,0,0,0,A_180,0,0,0,
							0,0,0,0,0,0,0,0,A_180,0,0,0,
							A_90,A_90,A_90,A_90,A_90,A_90,A_90,A_90,A_180,0,0,0,
							0,0,0,0,0,0,0,0,0,0,0,0};
		map = map_create(tiles_list, 5, map_plan, map_angle, 12, 6, id, objects_number);
	}
	else if(x == 4 && y == 1) {
		int objects_number = 0;
		Object* objects[2];
		Tiles_name tiles_list[] = {SAND, GRASS, DIRT};
		char map_plan[] = {0,1,1,1,1,0,1,0,1,0,2,1,
							0,1,1,1,1,2,1,0,1,0,1,2,
							0,1,2,1,1,2,1,0,1,0,1,2,
							0,1,1,1,1,0,1,0,1,2,1,2,
							0,0,1,1,1,2,1,0,1,0,1,2,
							0,1,1,1,1,0,1,0,1,0,2,0};
		char map_angle[] = {0,0,0,0,0,0,0,0,0,0,0,0,
							0,0,0,0,0,0,0,0,0,0,0,0,
							0,0,0,0,0,0,0,0,0,0,0,0,
							0,0,0,0,0,0,0,0,0,0,0,0,
							0,0,0,0,0,0,0,0,0,0,0,0,
							0,0,0,0,0,0,0,0,0,0,0,0};
		map = map_create(tiles_list, 3, map_plan, map_angle, 12, 6, id, objects_number);
	}
	else if(x == 4 && y == 0) {
		int objects_number = 0;
		//Object* objects[2];
		Tiles_name tiles_list[] = {GRASS, SAND, DIRT};
		char map_plan[] = {0,1,1,1,1,0,1,0,1,0,2,1,
							0,1,1,1,1,2,1,0,1,0,1,2,
							0,0,1,1,1,2,1,0,1,0,1,2,
							0,1,1,1,1,0,1,0,1,2,1,2,
							0,0,1,1,1,2,1,0,1,0,1,2,
							0,1,1,1,1,0,1,0,1,0,2,0};
		char map_angle[] = {0,0,0,0,0,0,0,0,0,0,0,0,
							0,0,0,0,0,0,0,0,0,0,0,0,
							0,0,0,0,0,0,0,0,0,0,0,0,
							0,0,0,0,0,0,0,0,0,0,0,0,
							0,0,0,0,0,0,0,0,0,0,0,0,
							0,0,0,0,0,0,0,0,0,0,0,0};
		map = map_create(tiles_list, 3, map_plan, map_angle, 12, 6, id, objects_number);
	}
	else return NULL;

	if(!map) return NULL;

	map->tileset = load_map_tileset(map);
	if(!map->tileset /*||!map->objects*/) {
		map_free(map);
		return NULL;
	}

	return map;
}

Map* map_create(const Tiles_name* tiles_list, short tiles_number, const char* map_plan, const char* map_angle, short width, short height, int id, int objects_number)
{
	Map* map = malloc(sizeof(Map));
	if(!map) return NULL;

	map->tiles_list = malloc(sizeof(Tiles_name)*tiles_number);
	if(!map->tiles_list) {
		map_free(map);
		return NULL;
	}

	map->map = malloc(sizeof(char)*width*height);
	map->map_angle = malloc(sizeof(unsigned char)*width*height);
	map->map_objects = malloc(width*height*sizeof(unsigned char));
	memset(map->map_objects, 0, width*height*sizeof(unsigned char));

	map->objects = NULL;

	if(!map->map || !map->map_angle || !map->map_objects) {
		map_free(map);
		return NULL;
	}

	map->is_drawn = 0;
	map->id = id;

	map->width = width;
	map->height = height;

	map->tiles_number = tiles_number;

	map->objects_number = objects_number;

	memcpy(map->tiles_list, tiles_list, sizeof(Tiles_name)*tiles_number);
	memcpy(map->map, map_plan, sizeof(char)*width*height);
	memcpy(map->map_angle, map_angle, sizeof(char)*width*height);
	
	return map;
}

void map_add_refresh_zone(Map* map, short y1, short y2, short v_size)
{
	if(map->update_number < MAX_UPDATE_ZONE-2) {
		map->update[map->update_number] = MIN(y1, y2)*(MIN(y1, y2)>=0);
		map->update[map->update_number+1] = MAX(y1, y2) + v_size;
		map->update_number+=2;
	}
}

// The weird cases (but not so rare :S) are not handled yet, but I made that in 10 minutes ^^
// Fusions supperposed zones to reduce transfer time. 
void map_refresh_zone_optimize(Map* map)
{
	int i, j, y1, y2;
	for(i=0; i< map->update_number; i+=2) {
		y1 = map->update[i];
		y2 = map->update[i+1];
		for(j=i+2; j< map->update_number; j+=2) {
			if(y1 <= map->update[j] && map->update[j] <= y2) {
				if(map->update[j+1] >= y2)
					map->update[i+1] = map->update[j+1];
				
				memmove(map->update+j, map->update+j+2, sizeof(short)*(map->update_number-j));
				map->update_number -= 2;
			}
			else if(y1 <= map->update[j+1] && y2 >= map->update[j+1]) {
				if(map->update[j] <= y1)
					map->update[i] = map->update[j];

				memmove(map->update+j, map->update+j+2, sizeof(short)*map->update_number);
				map->update_number -= 2;
			}
		}
	}
}

// x and y are pixels on the screen
int get_cell_id(short x, short y, Map* map)
{
	return (int)((x / TILE_SIZE)+map->width*(y / TILE_SIZE));
}

void map_set_cell_content(Map* map, Object* object, int content)
{
	int i, j, base;
	int line_nb, column_nb;

	column_nb = get_cell_id(object->x+object->tile->width, object->y, map) - get_cell_id(object->x, object->y, map) + 1;
	line_nb = (get_cell_id(object->x, object->y+object->tile->height, map) - get_cell_id(object->x, object->y, map))/map->width + 1;

	base = get_cell_id(object->x, object->y, map);

	for(i=0; i < line_nb; i++) {
		for(j=0; j < column_nb; j++) {
			map->map_objects[base + i*map->width + j] = content; 
		}
	}
}

void draw_refresh_map(Map* map, Player* player, char* str)
{
	int i;

	map_refresh_zone_optimize(map);

	//Optimisation possible : check if object is in some refresh zone before
	//redrawing it.
	for(i=0; i< map->objects_number; i++) draw_object(map->objects[i]);
	
	text_print(10, 200, str, 2, 0x0000FFFF);
}

void map_free(Map* map)
{
	if(map->tileset) free_map_tileset(map);
	if(map->tiles_list) free(map->tiles_list);
	if(map->map) free(map->map);
	if(map->map_angle) free(map->map_angle);
	if(map->map_objects) free(map->map_objects);
	if(map->objects) free_map_objects(map); // System error 2 ->

	free(map);
}