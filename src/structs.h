#ifndef STRUCTS_H

#define STRUCTS_H

#define THEPROG 1
#define PLAYER_SPEED_THEPROG 1

#define EMULATOR 1

// Max number of zone to be updated on the map when refreshing the screen
#define MAX_UPDATE_ZONE 10

typedef enum {
	// TILES
	GRASS = 0, DIRT = 1, SAND = 2, BITUMEN = 3, SNOW = 4, CORN = 5, WOODEN_FLOOR = 6,// To Fill
	DIRT_GRASS_LINE = 7, DIRT_GRASS_TURN_INT = 8, DIRT_GRASS_TURN_EXT = 9,
	SAND_GRASS_LINE = 10, SAND_GRASS_TURN_INT = 11, SAND_GRASS_TURN_EXT = 12,
	BITUMEN_GRASS_LINE = 13, BITUMEN_GRASS_TURN_INT = 14, BITUMEN_GRASS_TURN_EXT = 15,
	// CHARACTERS
	PLAYER_PILOT_UP, PLAYER_PILOT_RIGHT, PLAYER_PILOT_DOWN, PLAYER_PILOT_LEFT,
	PLAYER_GEORGES_UP, PLAYER_GEORGES_RIGHT, PLAYER_GEORGES_DOWN, PLAYER_GEORGES_LEFT,
	PLAYER_MECANO_UP, PLAYER_MECANO_RIGHT, PLAYER_MECANO_DOWN, PLAYER_MECANO_LEFT,
	SIMPLE_PILOT_UP, SIMPLE_PILOT_RIGHT, SIMPLE_PILOT_DOWN, SIMPLE_PILOT_LEFT,
	SIMPLE_MECANO_UP, SIMPLE_MECANO_RIGHT, SIMPLE_MECANO_DOWN, SIMPLE_MECANO_LEFT,
	SIMPLE_GIRL_UP, SIMPLE_GIRL_RIGHT, SIMPLE_GIRL_DOWN, SIMPLE_GIRL_LEFT,
} Tiles_name; //List of all the Tiles Names that can be loaded.

typedef enum {
	OBJECT_NONE = -1, TABLE = 0, 
	HOUSE_POKEMON, HOUSE_BAMBOO, HOUSE_CITY, LITTLE_HOUSE,
	JOURNAL, PLACARD, TABLE_PLAN, DESK, SHELVES, WOOD_TABLE, HANGAR, TREE, PLANE_SIDE_1,
	BARRIERE_HORIZONTAL, BARRIERE_VERTICAL,
	PNJ_MARKER, // All the PNJ's should be defined after that "marker" !!!
	PNJ_PILOT_UP, PNJ_PILOT_RIGHT, PNJ_PILOT_DOWN, PNJ_PILOT_LEFT,
	PNJ_MECANO_UP, PNJ_MECANO_RIGHT, PNJ_MECANO_DOWN, PNJ_MECANO_LEFT,
	PNJ_GIRL_UP, PNJ_GIRL_RIGHT, PNJ_GIRL_DOWN, PNJ_GIRL_LEFT,
} Objects_name;

typedef enum {
	UP = 0, RIGHT = 1, DOWN = 2, LEFT = 3, NONE = 4,
} Direction;

typedef enum {
	QUEST_UNCOMPLETED = 0, QUEST_NEW = 1,
} Quest_Status;

typedef enum {
	STATIC = 0, MOVING = 1,
} Status;

//sizeof(Tile) == 12 bytes
typedef struct {
	short width, height;
	unsigned char* bmp;
	short* palette;
} Tile; //Unique Tile Info

typedef struct {
	Objects_name name;
	int map_id;
	int unique_id; //Don't know how this'll be implemented ...
	short flag;
	short x, y;
	char* text;
	char interact_count;
	//To define : characteristics (movable ? effect ?.
	Tile* tile;
} Object; //To complete

typedef struct {
	short id;
	char consulted; //Maybe pretty useless ?
	char* text;
	//void* infos;	
} Quest;

//sizeof(Character) = 20 bytes
typedef struct {
	char anim, anim_max;
	int anim_ticks;
	short width, height;
	short tiles_number;
	Tile* current_tile;
	unsigned char* current_tile_base;
	Tile** tiles;
} Character;

typedef struct {
	short x, y;
	int map_id;
	short colision_tolerance[4];
	int move_ticks;
	Character* character;
	Direction direction;
	Status status;
} Player;

// The following structure (Map) needs to be cleaned and
// completed with incomming features (such as Objects, PNJ's...)
typedef struct
{
	char is_drawn;
	int id;
	short height, width; //Size in standard tiles
	
	Object** objects;
	short objects_number;

	char update_number;
	short update[2*MAX_UPDATE_ZONE];

	Tiles_name* tiles_list; //List of used tiles in this map
	short tiles_number;
	Tile** tileset; //Tiles pointer list

	unsigned char* map_objects;
	unsigned char* map;
	unsigned char* map_angle;
} Map;

#endif //STRUCTS_H