#include <stdlib.h>

#include "structs.h"

#include "file.h"
#include "save.h"
#include "story.h"

void save_init()
{
	int handle;
	if(CC_make_folder(MAIN_FOLDER)==0) {
		CC_make_file(PLAYER_SAVE, sizeof(PLAYER_SAVE_TYPE)*PLAYER_SAVE_SIZE);
		CC_make_file(STORY_SAVE, sizeof(STORY_SAVE_TYPE)*STORY_SAVE_SIZE);
	} else {
		handle = CC_open_file(PLAYER_SAVE);
		if(handle<0) {
			short zero[PLAYER_SAVE_SIZE] = {0};
			CC_make_file(PLAYER_SAVE, sizeof(PLAYER_SAVE_TYPE)*PLAYER_SAVE_SIZE);
			handle = CC_open_file(PLAYER_SAVE);
			CC_write_file(handle, zero, PLAYER_SAVE_SIZE*sizeof(PLAYER_SAVE_TYPE));
		}
		CC_close_file(handle);

		handle = CC_open_file(STORY_SAVE);
		if(handle<0) {
			int zero = 0;
			CC_make_file(STORY_SAVE, sizeof(STORY_SAVE_TYPE)*STORY_SAVE_SIZE);
			handle = CC_open_file(STORY_SAVE);
			CC_write_file(handle, &zero, 1*sizeof(int));
		} 
		CC_close_file(handle);
	}
}

void save_player(Player* player)
{
	int handle;
	short player_data[4];
	short map_x = player->map_id >> 16; //id is x<<16|y, so get x and y back
	short map_y = player->map_id&0x00FF;

	player_data[0] = map_x;
	player_data[1] = map_y;
	player_data[2] = player->x;
	player_data[3] = player->y;

	handle = CC_open_file(PLAYER_SAVE);
	if(handle<0) return;
	CC_write_file(handle, player_data, 4*sizeof(short));
	
	CC_close_file(handle);
}

void save_story()
{
	int handle;
	int story_state = get_story_state();

	handle = CC_open_file(STORY_SAVE);
	if(handle<0) return;
	CC_write_file(handle, &story_state, 1*sizeof(int));

	CC_close_file(handle);
}

int save_load_story_state()
{
	int story_state;
	int handle;
	handle = CC_open_file(STORY_SAVE);
	if(handle<0) return -1;
	CC_read_file(handle, &story_state, 1, CURRENT_POSITION);

	CC_close_file(handle);

	return story_state;
}