#ifndef _GRAPHIC_FUNCTIONS
#define _GRAPHIC_FUNCTIONS

#define LCD_WIDTH_PX 384
#define LCD_HEIGHT_PX 216

#define GAME_WIDTH_PX 384
#define GAME_HEIGHT_PX 216

#define LCD_GRAM 0x202
#define LCD_BASE	0xB4000000
#define VRAM_ADDR 0xA8000000
#define SYNCO() __asm__ volatile("SYNCO\n\t":::"memory");
// Module Stop Register 0
#define MSTPCR0	(volatile unsigned *)0xA4150030
// DMA0 operation register
#define DMA0_DMAOR	(volatile unsigned short*)0xFE008060
#define DMA0_SAR_0	(volatile unsigned *)0xFE008020
#define DMA0_DAR_0  (volatile unsigned *)0xFE008024
#define DMA0_TCR_0	(volatile unsigned *)0xFE008028
#define DMA0_CHCR_0	(volatile unsigned *)0xFE00802C

void *GetVRAMAdress(void);
void FillVRAM(short color);
void Pixel(int x, int y, short color);
void HorizontalLine(int y, int x1, int x2, unsigned short color);
//Alpha = 0 => transparent // Alpha = 32 => opaque
void HorizontalLineAlpha(int y, int x1, int x2, short color, char alpha);
void Line(int x1, int y1, int x2, int y2, short color);
void Polygon(const int *x, const int *y, int nb_vertices, short color);
void FilledConvexPolygon(const int* x, const int* y, int nb_vertices, short color);
//angle == 0 || angle == 90 || angle == 180 || angle == 270 (clockwise)
void CopySpriteMasked(const unsigned char* bitmap, const short* palette, int x, int y, int width, int height, short mask, int angle);
void CopySprite4bitMasked(const unsigned char* bitmap, const short* palette, int x, int y, int width, int height, short mask);
// x and y = coin haut gauche
void RoundedSquareAlpha(int x, int y, int w, int h, int radius, short color, char alpha);
void Circle(int x, int y, int radius, short color);
void FilledCircle(int x, int y, int radius, short color);
void FilledRectangle(int x1, int y1, int x2, int y2, int border_width, short border_color, short fill_color);

//void DmaWait(void);
void RefreshScreen(void);

#endif //_GRAPHIC_FUNCTIONS
