#ifndef OBJECTS_H

#define OBJECTS_H

#include "structs.h"

typedef struct {
	short x, y; //object relative
	short radius;
} Interaction_zone;

#define MAX_OBJECTS 10

#define OBJECT_FLAG_PHYSIC 0x0001 // Can we pass through the object ?
#define OBJECT_FLAG_MOVABLE 0x0002 // Can we move the object ?
#define OBJECT_FLAG_INTERACT 0x0004 // Can we interact the object ?

Object* object_load(Objects_name name, short x, short y, int map_id, int unique_id, short flag);

void object_map_copy(Object** objects, Map* map, int objects_number);
void object_free(Object* object);
void object_set_text(Object* object, char* text);

Object* object_get_nearest_interaction(Map* map, Player* player);
int object_can_interact(Object* object, Player* player);
Interaction_zone object_get_interaction_zone(const Object* object);

int object_collision(Object* object, Player* player);

void free_map_objects(Map* map);

#endif //OBJECTS_H