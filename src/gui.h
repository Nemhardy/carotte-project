#ifndef GUI_H

#define GUI_H

#include "key.h"

//To continue and determine
#define GUI_KEY_QUEST K_F2
#define GUI_KEY_MAP K_F3

void gui_draw_game_zone(Map* map, Player* player);
void gui_draw_bar();

void gui_update();
void gui_draw_quest();

#endif //GUI_H