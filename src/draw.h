#ifndef DRAW_H

#define DRAW_H

#include "tiles.h"

#define STANDARD_MASK 0xf81a

// Global pointer to the differents "standard"
// tiles arround
extern Tile** standard_tileset;

// Loads the "standard" tiles into memory
// If process fails, returns NULL.
// Actually using heap, but will probably use
// a static memory based allocation (ie memmgr_)
int draw_init();

// Frees the "standard" tiles from memory
void draw_quit();

void draw_tileset_map(Map* map);

void draw_tile(Tile* tile, short x, short y);

void draw_character(Character* character, short x, short y);

#endif //DRAW_H