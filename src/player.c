#include <stdlib.h>
#include <RTC_syscalls.h>

#include "player.h"

#include "character.h"
#include "map.h"
#include "objects.h"
#include "tiles.h"
#include "key.h"
#include "structs.h"
#include "graphic_functions.h"


Player* player_load(int id, short x, short y)
{
	Player* player = malloc(sizeof(Player));
	if(!player) return NULL;

	player->character = character_load(id);
	if(!player->character) {
		free(player);
		return NULL;
	}

	player->x = x;
	player->y = y;

	player->colision_tolerance[UP] = 20;
	player->colision_tolerance[RIGHT] = 10;
	player->colision_tolerance[DOWN] = 0;
	player->colision_tolerance[LEFT] = 10;

	player->move_ticks = RTC_GetTicks();

	player->status = STATIC;
	player->direction = UP;

	return player;
}

void player_set_coord(Player* player, short x, short y, int map_id)
{
	player->x = x;
	player->y = y;
	player->map_id = map_id;
}

void player_update(Player* player, Map* map)
{
	static const int keyboard[4] = {KEY_UP, KEY_RIGHT, KEY_DOWN, KEY_LEFT}; // Placed in Read only for fastest access
	const Direction directions[4] = {UP, RIGHT, DOWN, LEFT};

	if(key_down(KEY_INTERACT)) player_interact(player, map); 

	if(key_down(keyboard[player->direction])) player->status = MOVING;
	else {
		int key = 0, i;
		for(i = 0; i<4; i++) 
			if(key_down(keyboard[i])) { 
				key = keyboard[i];
				break;
			}

		if(!key) {
			player->status = STATIC;
			player->move_ticks = RTC_GetTicks(); //TODO : try to optimize here
			map_add_refresh_zone(map, player->y, player->y, player->character->height);
			character_reset_frame(player->character);
		}
		else {
			player->status = MOVING;
			player->direction = directions[i];
			character_set_tile(player->character, player->direction);
		}

	}

	if(player->status == MOVING) {
		int y_old = player->y;
		character_next_frame(player->character);
		player_move(player, player->direction, map);
		map_add_refresh_zone(map, y_old, player->y, player->character->height);
	}
}

void player_interact(Player* player, Map* map)
{
	Object* object = NULL;
	if(!player_check_interaction(player, map)) return;

	object = object_get_nearest_interaction(map, player);

	if(!object) return;

	object->interact_count++; 

	if(object->name > PNJ_MARKER) pnj_draw_text(object);
}

void pnj_draw_text(Object* object)
{
	int size = 1, key, x, y;
	int width = text_width_line(object->text)*size;
	int height = text_height_line(object->text)*size;

	x = object->x + (object->tile->width/2) - (width/2);
	if(x<=10) x = 10;
	y = object->y - height - text_height_line("a")*size;

	RoundedSquareAlpha(x-text_widthC(' ')*size-1, y-size*2-1, width+2*text_widthC(' ')*size+2, height+text_height_line("a")*size+2, 3, 0x0000, 32);
	RoundedSquareAlpha(x-text_widthC(' ')*size, y-size*2, width+2*text_widthC(' ')*size, height+text_height_line("a")*size, 3, 0xFFFF, 32);
	text_print(x, y, object->text, size, 0x00000000);

	Bdisp_PutDisp_DD();
	OS_InnerWait_ms(250);
	GetKey(&key);
	DrawFrame(0x0000);
}

int player_check_interaction(Player* player, Map* map)
{
	int i, number_interactions = 0;
	for(i=0; i<map->objects_number; i++) {
		if(map->objects[i]->flag&OBJECT_FLAG_INTERACT) {
			if(object_can_interact(map->objects[i], player)) number_interactions++;
		}
	}
	return number_interactions;
}

void player_move(Player* player, Direction direction, Map* map)
{
	int vx = 0, vy = 0;

	//direction&1 is true when direction == RIGHT or LEFT
#ifdef THEPROG
	if(direction&1) vx = (PLAYER_SPEED_THEPROG) * ((player->direction &~1) ? -1 : 1);
	else vy = (PLAYER_SPEED_THEPROG) * ((player->direction)? 1 : -1);
#else
	if(direction&1) vx = (int)(((float)(RTC_GetTicks()-player->move_ticks) / 128.)*(float)PLAYER_SPEED) * ((player->direction &~1) ? -1 : 1);
	else vy = (int)(((float)(RTC_GetTicks()-player->move_ticks) / 128.)*(float)PLAYER_SPEED) * ((player->direction)? 1 : -1);
#endif
	if(vx || vy) player->move_ticks = RTC_GetTicks(); // To make the game usuable on the emulator with an high fps (>100 !)

	player->x += vx;
	player->y += vy;

	if(map->objects_number) player_colision(player, map);
}

void player_colision(Player* player, Map* map)
{
	int i;
	int player_cell[4];
	Object* object_tmp;

	//get the 4 cells where the player sprite is drawn (4 is the max as sprites are 32*32) 
	//for(i=0; i < 4; i++) player_cell[i] = get_cell_id(player->x+(!i^1)*player->character->width, player->y+(i>2)*player->character->height, map);

	player_cell[0] = get_cell_id(player->x, player->y, map);
	player_cell[1] = get_cell_id(player->x+player->character->width, player->y, map)*(player->x+player->character->width < GAME_WIDTH_PX);
	player_cell[2] = get_cell_id(player->x, player->y+player->character->height, map)*(player->y+player->character->height < GAME_HEIGHT_PX);
	player_cell[3] = get_cell_id(player->x+player->character->width, player->y+player->character->height, map)*(player->x+player->character->width < GAME_WIDTH_PX)*(player->y+player->character->height < GAME_HEIGHT_PX);
	
	for(i=1; i<4; i++) if(!player_cell[i]) player_cell[i] = player_cell[0];

	for(i=0; i < 4; i++) {
		object_tmp = map->objects[map->map_objects[player_cell[i]]];
		if((object_tmp->flag&OBJECT_FLAG_PHYSIC)&& object_collision(object_tmp, player)) {
			if(player->direction == UP) {
				player->y = object_tmp->y + object_tmp->tile->height - player->colision_tolerance[UP];
			} 
			else if(player->direction == RIGHT) {
				player->x = object_tmp->x - player->character->width + player->colision_tolerance[RIGHT];
			}
			else if(player->direction == DOWN) {
				player->y = object_tmp->y - player->character->height + player->colision_tolerance[DOWN];
			}
			else if(player->direction == LEFT) {
				player->x = object_tmp->x + object_tmp->tile->width - player->colision_tolerance[LEFT];
			}
		}
	}
}

void player_free(Player* player)
{
	character_free(player->character);
	free(player);
}