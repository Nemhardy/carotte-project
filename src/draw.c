#include <stdlib.h>

#include "draw.h"
#include "graphic_functions.h"
#include "tiles.h"

Tile** standard_tileset;

int draw_init()
{
	int i;
	standard_tileset = malloc(sizeof(Tile*)*LAST_STANDARD_TILE);
	
	standard_tileset[GRASS] = tile_load(GRASS);
	standard_tileset[DIRT] = tile_load(DIRT);
	standard_tileset[SAND] = tile_load(SAND);

	for(i=0; i< LAST_STANDARD_TILE; i++)
		if(!standard_tileset[i]) return 0;

	return 1;
}

void draw_quit()
{
	int i;
	for(i=0; i< LAST_STANDARD_TILE; i++)
		tile_free(standard_tileset[i]);

	free(standard_tileset);
}

void draw_tileset_map(Map* map)
{
	int i, j, angle;
	FillVRAM(0x0000); //To keep ?
	for(i=0; i < map->height; i++) {
		for(j=0; j < map->width; j++) {
			angle = 90*map->map_angle[i*map->width + j];
			CopySpriteMasked(map->tileset[map->map[i*map->width + j]]->bmp, map->tileset[map->map[i*map->width + j]]->palette, TILE_SIZE*j, TILE_SIZE*i, TILE_SIZE, TILE_SIZE, 0x0000, angle);
		}
	}
	map->is_drawn = 1;
	SaveVRAM_1();// To add : save_vram();
}

void draw_tile(Tile* tile, short x, short y)
{
	CopySpriteMasked(tile->bmp, tile->palette, x, y, tile->width, tile->height, STANDARD_MASK, 0);
}

void draw_character(Character* character, short x, short y)
{
	CopySpriteMasked(character->current_tile->bmp, character->current_tile->palette, x, y, character->width, character->height, STANDARD_MASK, 0);
	//draw_tile(character->current_tile, 50, 50/*character->x, character->y*/);
}

void draw_object(Object* object)
{
	draw_tile(object->tile, object->x, object->y);
}

void draw_player(Player* player)
{
	draw_character(player->character, player->x, player->y);
}