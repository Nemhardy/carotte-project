#include <stdlib.h>

#include "map.h"
#include "player.h"
#include "objects.h"
#include "structs.h"
#include "graphic_functions.h"

Direction get_map_quit_direction(Player* player);
void engine_refresh_screen(Map* map);

extern int getFps();

void engine_main()
{
	int error_key = 0, running = 1, nb_map = 0, number_interactions = 0;
	char error_nb[] = "**********";
	Direction direction_quit = NONE;
	Map* map = NULL;
	Player* player = NULL;
	Object* object_tmp = NULL;
	short player_buf_x = 50, player_buf_y = 50;
	short map_x = 2, map_y = 1;
	int* test;
	short turn = 1;
	//To add here : save and loading from saves.

	draw_init();
	init_object();

	player = player_load(-1, player_buf_x, player_buf_y); //Can fail !?
	

	while(running) {
		map = map_load(map_x, map_y);
		nb_map++;
		
		player_set_coord(player, player_buf_x, player_buf_y, map->id);

		if(!map) {
			FillVRAM(0xFFFF);
			itoa(nb_map, error_nb+2);
			text_print(10,180, error_nb, 2, 0x00000000);
			text_print(10, 200, "Error loading(engine_main()->map)", 2, 0x00000000);
			while(1) {
				GetKey(&error_key); //Safequit if any error arrived in the mem allocation
			}
		}
		if(!player) {
			FillVRAM(0xFFFF);
			itoa(nb_map, error_nb+2);
			text_print(10,180, error_nb, 2, 0x00000000);
			text_print(10, 200, "Error loading(engine_main()->player)", 2, 0x00000000);
			while(1) {
				GetKey(&error_key); //Safequit if any error arrived in the mem allocation
			}
		}

		if(!map->is_drawn)draw_tileset_map(map);
		else LoadVRAM_1();

		map_add_refresh_zone(map, 0, 0, GAME_HEIGHT_PX);
		draw_refresh_map(map, player, error_nb);

		while(get_map_quit_direction(player) == NONE) {
			object_tmp = object_get_nearest_interaction(map, player);
			
			player_update(player, map);

			quest_check_completed(player, map);
			
			LoadVRAM_1();

			gui_update();

			map_add_refresh_zone(map, 200, 200, 15);
			memset(error_nb, '*', 10);
			//itoa(getFps(), error_nb+2);
			itoa((object_tmp!=NULL), error_nb+2);

			draw_refresh_map(map, player, error_nb);
			draw_player(player);
			gui_draw_game_zone(map, player);
			engine_refresh_screen(map);
			if(key_down(47)) {
				running = 0;
				break;				
			}
		}

		direction_quit = get_map_quit_direction(player);

		map_free(map);

		player_buf_y = (0-(int)(player->character->height/2))*(direction_quit==DOWN) + (GAME_HEIGHT_PX-(int)(player->character->height/2))*(direction_quit==UP) + player->y*(direction_quit==LEFT || direction_quit==RIGHT);
		player_buf_x = (0-(int)(player->character->width/2))*(direction_quit==RIGHT) + (GAME_WIDTH_PX-(int)(player->character->height/2))*(direction_quit==LEFT) + player->x*(direction_quit==UP || direction_quit==DOWN);
		map_x = map_x + (direction_quit==RIGHT) - (direction_quit==LEFT);
		map_y = map_y + (direction_quit==DOWN) - (direction_quit==UP);

		direction_quit = NONE;
		turn++;
	}

	player_free(player);

	draw_quit();
}

Direction get_map_quit_direction(Player* player)
{
	short p_width = player->character->width;
	short p_height = player->character->height;

	if(player->x + (int)(p_width/2) < 0 && player->direction == LEFT) return player->direction;
	else if(player->x + (int)(p_width/2) > GAME_WIDTH_PX && player->direction == RIGHT) return player->direction;
	else if(player->y + (int)(p_height/2) < 0 && player->direction == UP) return player->direction;
	else if(player->y + (int)(p_height/2) > GAME_HEIGHT_PX && player->direction == DOWN) return player->direction;
	else return NONE;
}

void engine_refresh_screen(Map* map)
{
	#ifdef EMULATOR
	Bdisp_PutDisp_DD();
#else
	for(i = 0; i < map->update_number; i+=2)
		Bdisp_PutDisp_DD_stripe(map->update[i], map->update[i+1]);
#endif

	memset(map->update, 0, MAX_UPDATE_ZONE*2*sizeof(short));
	map->update_number = 0;
}