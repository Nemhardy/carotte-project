#include <stdlib.h>
#include <RTC_syscalls.h>

#include "draw.h"
#include "map.h"
#include "graphic_functions.h"
#include "player.h"
#include "text.h"
#include "memmgr.h"
#include "key.h"

int main_menu();

int getFps()
{
    static int disp_fps=0, fps=1, time=0;

    if(RTC_GetTicks() - time > 128)
    {
        disp_fps = fps;
        fps = 0;
        time = RTC_GetTicks();
    }
    fps++; // on monte la valeur des FPS

    return disp_fps;
}

int main()
{
	int key = 0, menu;

	//Enable full (16 bit) colors
	Bdisp_EnableColor(1);

	//Disable status area when using GetKey
	EnableStatusArea(3); 

	DrawFrame(0x0000);
	FillVRAM(0xFFFF);
	text_print(10, 100, "Carotte Studio - 2014", 3, 0x00000000);
	text_print(10, 130, "Appuyez sur une touche", 2, 0x00000000);

	GetKey(&key);
	//Fill the border of the screen of black.
	DrawFrame(0x0000);

	menu = main_menu(); //Assuming we'll code it :).

	switch(menu) {
		case 1:
			game_main();
			break;
		default:
			break;
	}
	//first_play();

	memmgr_init();
	engine_main();

	while(1) {
		GetKey(&key);
	}
}

void game_main()
{
	int story_state;

	save_init();
	load_story_state_from_save();

	story_state = get_story_state();
	if(story_state == 0) {
		first_play();
		set_story_state(1);
	}
	else engine_main();
}

int main_menu()
{
	return 1;
}

void first_play()
{
	int key = 0;
	int x = 20, y = 100, size = 2;

	FillVRAM(0x0000);

	text_print(x, y, "- Tu es sur que c'est le bon moment ?", size, 0x0000FFFF);
	GetKey(&key);
	first_play_fondu(x, y, "- Tu es sur que c'est le bon moment ?", size, 0x0000);

	text_print(x, y, "- Mais oui, puisque je te le dis !\nLa derniere fois qu'on entend parler\nde lui par chez nous, c'est fin 1918.", size, 0x0000FFFF);
	GetKey(&key);
	first_play_fondu(x, y, "- Mais oui, puisque je te le dis !\nLa derniere fois qu'on entend parler\nde lui par chez nous, c'est fin 1918.", size, 0x0000);
	
	text_print(x, y, "- Ca je le sais, mais quand bien meme \ntu ...", size, 0x0000FFFF);
	GetKey(&key);
	first_play_fondu(x, y, "- Ca je le sais, mais quand bien meme \ntu ...", size, 0x0000);

	text_print(x, y, "- Ne perdons plus de temps, fais ce \nque je t'ai dit et ca se passera \nbien !", size, 0x0000FFFF);
	GetKey(&key);
	first_play_fondu(x, y, "- Ne perdons plus de temps, fais ce \nque je t'ai dit et ca se passera \nbien !", size, 0x0000);

	text_print(x, y, "- Oui, mais laisse moi sortir au moins.", size, 0x0000FFFF);
	GetKey(&key);
	first_play_fondu(x, y, "- Oui, mais laisse moi sortir au moins.", size, 0x0000);

	text_print(x, y, "- Vas y, je suis pret, ne touche a \nrien si ce n'est le gros levier sous \nle cadran.", size, 0x0000FFFF);
	GetKey(&key);
	first_play_fondu(x, y, "- Vas y, je suis pret, ne touche a \nrien si ce n'est le gros levier sous \nle cadran.", size, 0x0000);

	OS_InnerWait_ms(1000); //Some kind of supsense ! :)

	
	return;
}

void first_play_fondu(int x, int y, char* text, int size, short color)
{
	int alpha, real_color;

	for(alpha = 31; alpha>=0; alpha--) {
		real_color = (alpha<<16)|(color);
		text_print(x, y, text, size, 0x0000FFFF);
		text_print(x, y, text, size, real_color);
		Bdisp_PutDisp_DD();
		OS_InnerWait_ms(25); //To check on real calc.
	}
}