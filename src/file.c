#include <stdlib.h>
#include <string.h>

#include "file.h"

extern int Bfile_ReadFile_OS( int HANDLE, void *buf, int size, int readpos ); // Quick fix because of a not working syscall call

void CC_make_path(unsigned short* destination, const unsigned char* file)
{
	unsigned char* buffer = malloc(sizeof(unsigned char)*(strlen(file)+strlen(ROOT)));
	memcpy(buffer, (unsigned char*)ROOT, sizeof(unsigned char)*sizeof(ROOT));
	strcat(buffer, file);
	CC_Sys_StrToName(destination, buffer, strlen(buffer));
	free(buffer);
}

// folder should be like "folder" or "old\\new" assuming "old" is already existing 
int CC_make_folder(unsigned char* folder)
{
	int ret;
	//ToDo : Add a path validation (and creation if doesn't exists)
	unsigned short* path = malloc((sizeof(ROOT)+strlen(folder))*sizeof(unsigned short));
	CC_make_path(path, folder);
	ret = CC_Sys_CreateEntry(path, CREATEMODE_FOLDER, 0);
	free(path);
	return ret; //ToDo : handle error 
}

// file should be like "file.txt" or "folder\\mario.sav" assuming folder is already existing
int CC_make_file(unsigned char* file, int size)
{
	//ToDo : Add a path validation (and creation if doesn't exists)
	int ret;
	int vsize = size;
	int* psize = &vsize;
	unsigned short* path = malloc((sizeof(ROOT)+strlen(file))*sizeof(unsigned short));
	CC_make_path(path, file);
	ret = CC_Sys_CreateEntry(path, CREATEMODE_FILE, psize);
	free(path);
	return ret;
}

int CC_open_file(unsigned char* file) //Mode is actually READWRITE
{
	int result;
	unsigned short* path = malloc((sizeof(ROOT)+strlen(file))*sizeof(unsigned short));
	CC_make_path(path, file);
	result = CC_Sys_OpenFile(path, READWRITE); //ToDo : add an error code gesture
	free(path);
	return result;
}

int CC_close_file(int handle)
{
	return CC_Sys_CloseFile(handle);
}

int CC_write_file(int handle, const void *buf, int size)
{
	return CC_Sys_WriteFile(handle, buf, size);
}

int CC_read_file(int handle, void* destination, int size, int position)
{
	return Bfile_ReadFile_OS(handle, destination, size, position);
}

/* ## MEMORY FUNCTIONS ## */

//0x1DDC
void CC_Sys_StrToName( unsigned short*dest, const unsigned char*source, int n )
{
	__asm__("mov.l CC_syscall_1, r2\n"
	    "mov.l CC_SysFile_StrToName, r0\n"
	    "jmp @r2\n"
	    "nop\n"
	    "CC_syscall_1: .long 0x80020070\n"
	    "CC_SysFile_StrToName: .long 0x1DDC");
}

//0x1DAE
int CC_Sys_CreateEntry( const unsigned short*filename, int mode, int*size )
{
	__asm__("mov.l CC_syscall_2, r2\n"
	    "mov.l CC_SysFile_CreateEntry, r0\n"
	    "jmp @r2\n"
	    "nop\n"
	    "CC_syscall_2: .long 0x80020070\n"
	    "CC_SysFile_CreateEntry: .long 0x1DAE");
}

//0x1DA3
int CC_Sys_OpenFile( const unsigned short* filename, int mode )
{
	__asm__("mov.l CC_syscall_3, r2\n"
	    "mov.l CC_SysFile_OpenFile, r0\n"
	    "jmp @r2\n"
	    "nop\n"
	    "CC_syscall_3: .long 0x80020070\n"
	    "CC_SysFile_OpenFile: .long 0x1DA3");
}


//0x1DAC Actually not working 0o
/*int CC_Sys_ReadFile( int HANDLE, void *buf, int size, int readpos ) 
{
	__asm__("mov.l CC_syscall_4, r2\n"
	    "mov.l CC_SysFile_ReadFile, r0\n"
	    "jmp @r2\n"
	    "nop\n"
	    //".align 2\n"
	    "CC_syscall_4: .long 0x80020070\n"
	    "CC_SysFile_ReadFile: .long 0x1DAC");
}*/

//0x1DA4
int CC_Sys_CloseFile( int HANDLE )
{
	__asm__("mov.l CC_syscall_5, r2\n"
	    "mov.l CC_SysFile_CloseFile, r0\n"
	    "jmp @r2\n"
	    "nop\n"
	    "CC_syscall_5: .long 0x80020070\n"
	    "CC_SysFile_CloseFile: .long 0x1DA4");
}

/*int CC_Sys_Read( int HANDLE, void *buf, int size, int readpos ) 
{
	__asm__("mov.l CC_syscall_7, r2\n"
	    "mov.l CC_SysFile_ReadFile, r0\n"
	    "jmp @r2\n"
	    "nop\n"
	    "CC_syscall_7: .long 0x80020070\n"
	    "CC_SysFile_ReadFile: .long 0x1DAC");
}*/

//0x1DAF
int CC_Sys_WriteFile( int HANDLE, const void *buf, int size )
{
	__asm__("mov.l CC_syscall_6, r2\n"
	    "mov.l CC_SysFile_WriteFile, r0\n"
	    "jmp @r2\n"
	    "nop\n"
	    "CC_syscall_6: .long 0x80020070\n"
	    "CC_SysFile_WriteFile: .long 0x1DAF");
}