#ifndef TILES_H

#define TILES_H

#include "structs.h"

// When talking of global tiles, we're talking
// Of tiles often used and almost never freed

// Define the global tiles number
#define LAST_STANDARD_TILE 3

// Tile from the main tileset in pixel (squares tiles)
#define TILE_SIZE 32

// Returns a Tile* array containing the tiles used in map 
Tile** load_map_tileset(const Map* map);

// Frees the non standard tiles used in map 
void free_map_tileset(const Map* map);

// Returns a pointer to a loaded Tile in the heap
Tile* tile_load(Tiles_name name);

void tile_free(Tile* tile);

// Probably designed to become private
Tile* tile_create(const unsigned char* sprite, const short* palette, short palette_lenght, short width, short height);

// Probably better as macro 
Tile* get_standard_tile(Tiles_name name);
int is_standard_tile(Tiles_name name);

#endif //TILES_H