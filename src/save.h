#ifndef SAVE_H

#define SAVE_H

#include "structs.h"

#define MAIN_FOLDER "carotte"

#define PLAYER_SAVE "carotte\\player.sav"
#define PLAYER_SAVE_TYPE short
#define PLAYER_SAVE_SIZE 4
// Data structure : player.sav : 
// int array : 
// current_map_x >> current_map_y >> on_map_x >> on_map_y >>

#define STORY_SAVE "carotte\\story.sav"
#define STORY_SAVE_TYPE int
#define STORY_SAVE_SIZE 1
// Data structure : story.sav
// int (array of one element)
// story_state

void save_init();
void save_player(Player* player);

#endif //SAVE_H