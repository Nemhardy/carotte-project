#ifndef MAP_H

#define MAP_H

#include "structs.h"

#define A_0 0
#define A_90 1
#define A_180 2
#define A_270 3

// Returns a pointer to an initialized map
// Needs to be filled with the different maps init stuff
// If loading fail, returns NULL
Map* map_load(short x, short y);

// Probably designed to become private
Map* map_create(const Tiles_name* tiles_list, short tiles_number, const char* map_plan, const char* map_angle, short width, short height, int id, int objects_number);

void map_add_refresh_zone(Map* map, short y1, short y2, short v_size);

void draw_refresh_map(Map* map, Player* player, char* str);

int get_cell_id(short x, short y, Map* map);

void map_set_cell_content(Map* map, Object* object, int content);

// Frees a map
void map_free(Map* map);

#endif //MAP_H