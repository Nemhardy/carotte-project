#ifndef PLAYER_H

#define PLAYER_H

#include "key.h"
#include "structs.h"

#define PLAYER_SPEED 100 // pixels/seconds

#define KEY_UP K_UP
#define KEY_RIGHT K_RIGHT
#define KEY_DOWN K_DOWN
#define KEY_LEFT K_LEFT
#define KEY_INTERACT K_SHIFT

Player* player_load(int id, short x, short y);
void player_set_coord(Player* player, short x, short y, int map_id);
void player_update(Player* player, Map* map);
void player_move(Player* player, Direction direction, Map* map);
void player_colision(Player* player, Map* map);
void player_free(Player* player);
int player_check_interaction(Player* player, Map* map);

#endif //PLAYER_H