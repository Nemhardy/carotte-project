#ifndef STORY_H

#define STORY_H

void load_story_state_from_save();
int story_objects_number(int id);
void story_objects_complete(int id, Object** objects, int standard_number);
Quest* quest_load(int id);
Quest* quest_create(int id, char* text);
void quest_free(Quest* quest);
short get_story_state();
const Quest* get_current_quest();
Quest_Status quest_check_completed(Player* player, Map* map);

#endif