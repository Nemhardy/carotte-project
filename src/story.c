#include <stdlib.h>
#include <string.h>

#include "structs.h"
#include "objects.h"
#include "story.h"

static int story_state = 0;
static Quest* current_quest = NULL;

void load_story_state_from_save()
{
	story_state = save_load_story_state();
	
	if(story_state<0) story_state = 0;

	current_quest = quest_load(story_state);
}

int story_objects_number(int id)
{
	short x = id >> 16; //id is x<<16|y, so get x and y back
	short y = id&0x00FF;

	if(x == 1 && y == 1) {
		if(story_state == 1) {
			return 1;
		}
	}

	return 0;
}

Player* story_get_player()
{
	short x = 50, y = 50;
	Player* player = NULL;
	
}

void set_story_state(int value)
{
	story_state = value;
}

void story_objects_complete(int id, Object** objects, int standard_number) 
{
	short x = id >> 16;
	short y = id&0x00FF;

	if(story_objects_number(id) == 0) return;

	if(x == 1 && y == 1) {
		if(story_state == 1) {
			objects[standard_number] = object_load(TABLE, 75, 115, id, 0, OBJECT_FLAG_PHYSIC|OBJECT_FLAG_MOVABLE);
		}
	}
	return;
}

Quest* quest_load(int id)
{
	Quest* quest = NULL;
	if(id == 0) {
		char text[] = "The magnifical quest n0 !\nUp to you\nRight now.";
		quest = quest_create(id, text);
	}
	else if(id == 1) {
		char text[] = "Ceci est la premiere quete de test.\nMettons la sur deux lignes pour voir !";
		quest = quest_create(id, text);
	} else if(id == 2) {
		char text[] = "Ceci est la seconde quete de test.\nMettons la sur deux lignes pour voir !";
		quest = quest_create(id, text);
	}


	if(!quest) return NULL;

	return quest;
}

Quest* quest_create(int id, char* text)
{
	Quest* quest = NULL;

	quest = malloc(sizeof(Quest));
	if(!quest) return NULL;

	quest->id = id;
	quest->consulted = 0;

	quest->text = malloc(strlen(text)+1);
	if(!quest->text) {
		free(quest);
		return NULL;
	}
	memcpy(quest->text, text, strlen(text)+1);


	return quest;
	//ToDo for the infos.
}

void quest_free(Quest* quest) 
{
	free(quest->text);
	free(quest);
}

short get_story_state()
{
	return story_state;
}

void quest_set_consulted() 
{
	current_quest->consulted = 1;
}

const Quest* get_current_quest()
{
	return current_quest;
}

//ToDo : there's actually a lot of tests, even when the quest is uncompleted
Quest_Status quest_check_completed(Player* player, Map* map)
{
	short x = map->id >> 16; //id is x<<16|y, so get x and y back
	short y = map->id&0x00FF;

	if(current_quest->id == 1) {
		if(x == 3 && y == 1) { //if x == 1 && y == 1
			quest_free(current_quest);
			story_state=2; // story_state ++;
			current_quest = quest_load(story_state);
			return QUEST_NEW;
		}
	}
	else if(current_quest->id == 2) {
		if(x == 5 && y == 1) { //if x == 1 && y == 1
			quest_free(current_quest);
			story_state=1; // story_state ++;
			current_quest = quest_load(story_state);
			return QUEST_NEW;
		}
	}
	return QUEST_UNCOMPLETED;
}